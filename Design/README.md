﻿# Links #

Tutoriais muito legais
1)waltermattos.com

Excelente página sobre design em geral, tem uma área de tutoriais muito boa
2)abduzeedo.com

Bom para inspirações
3)thebestdesigns.com

Sistema de venda e compartilhamento de icones
4)iconfinder.com

Gera gradientes e o css correspodente
5)colorzilla.com/gradient-editor/

Ótima fonte de inspiração
6)onextrapixel.com

Download de Grid para diversos softwares, incluindo PS, AI e FW
7)960.gs

Seletor de cores RGB e familias de cores, além de poder ver a escolha de cores de outros designers
8)color.adobe.com/pt/create/color-wheel/