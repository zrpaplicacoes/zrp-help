## ZRPGrids para os designers

### O que é?

É um sistema de GUIDES(GRIDS) e COLUMNS para a construção de páginas responsivas.
O arquivo do Illustrator introduz alterações no 960gs para se adaptar a responsividade do ZRPGrids, já que originalmente o 960gs foi feito para se adaptar ao Bootstrap.

### Como usar?

Basta fazer uma cópia do arquivo .ai na pasta do projeto, renomear e utilizar.

* 	A versão mínima do Illustrator para utilizar o ZRPGrids é a CS6.