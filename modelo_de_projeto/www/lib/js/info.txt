Os arquivos dentro da pasta lib s�o referentes a todas as bibliotecas ou ferramentas utilizadas na cria��o do site. Se tratarem de front end, DEVEM estar em sua vers�o minified, seguindo o mesmo padr�o de nomenclatura. Se forem libs de php, tratando de apenas uma classe, devem seguir o padr�o [NOME DA TABELA]_classes.php.
�	Ex1 � style (devem estar na forma minified): Folhas de estilo como do dataTable ou de um slideshow
�	Ex2 � js (devem estar na forma minified): Bibliotecas JQuery, plug-ins
�	Ex3 � php: Arquivo de conex�o (chamado de conexao.php), arquivo de classes para usu�rios

OBS: Caso seja apenas a implementa��o do plugin javascript, esse deve constar na p�gina js/[NOME DA PAGINA].js. Isso porque queremos separar o que � biblioteca do que � implementa��o.
