#ZRPWEBCREATIONS
### NOMEDOPROJETO

### Criação ###


| Tarefas                         |               Feito             |
| ------------------------------- |---------------------------------|
|Design Criado                    |                Não              | 
|Front-End Completo               |                Não              | 
|Back-End Completo                |                Não              |
|Checar todos os links            |                Não              |
|Meta Tags de busca				  |                Não              | 
|Ogg Tags para Facebook           |                Não              | 
|Page Speed Insights   			  |                Não              | 
|Comprimir Códigos JavaScripts    |                Não              | 
|Otimizar Imagens                 |                Não              | 
|Chrome                			  |                Não              | 
|Safari          			      |                Não              | 
|Mozila                			  |                Não              | 
|IE                               |                Não              | 
|http://netrenderer.com/          |                Não              | 
|.htcasses                        |                Não              | 
|Erro (404)                       |                Não              | 
| ------------------------------- |---------------------------------|

### Pós Projeto ###

| Tarefas                         |               Feito             |
| ------------------------------- |---------------------------------|
|Google, Bing, Yahoo              |                Não              | 
|Analytics                        |                Não              | 
|Divulgação?                      |                Não              |
|Feedback 				          |                Não              |

#### Programadores
Listar o nome dos programadores envolvidos no projeto
* Programador1
* Programador2