#ZRPWEBCREATIONS
### NOMEDOPROJETO
#### Escopo do projeto

Do que se trata o site, informações relevantes fornecidas pelo cliente, escolha de cores, etc

#### Páginas do projeto

* Home
* Item1
* Item2
    * subitem 1
    * subitem2
    * subitem3
* Item3

### Design ###

#### Softwares utilizados
* ADOBE X + VERSÃO
* COREL Y + VERSÃO

####Fontes

Lista das fontes com link
* Avenir Medium - [LINKPARAAFONTE1]
* Helvetica - [LINKPARAAFONTE2]
[LINKPARAAFONTE1]:http://www.myfonts.com/fonts/adobe/avenir/medium/
[LINKPARAAFONTE2]:http://www.fonts.com/font/linotype/helvetica

#### Códigos

Animações em SVG puro, etc..., quando existirem, devem vir aqui, ex:

```sh
<animateTransform attributeName="transform"
                        attributeType="XML"
                       	type="rotate"
                        from="0 60 70"
                        to="360 60 70"
                        dur="10s"
                        repeatCount="indefinite"/>
```
#### Imagens comerciais

Lista das imagens utilizadas, licença de imagem delas e link para a original
* Paisagem 1 - CC BY-SA-... - [LINK] 
* Paisagem 2 - RM - [LINK2]
[LINK]:http://www.pexels.com/photo/3919/
[LINK2]:http://www.cutedrop.com.br/2013/07/entendendo-as-licencas-de-uso-de-imagens/

#### Outros

Caso algum plugin, brush, pattern, etc..., seja utilizado, deve ser listado aqui, no mesmo modelo das imagens,
* Plugin 1 - CC BY-SA-... - [LINK] 
* Plugin 2 - RM - [LINK2]
[LINK]:http://www.pexels.com/photo/3919/
[LINK2]:http://www.cutedrop.com.br/2013/07/entendendo-as-licencas-de-uso-de-imagens/

#### Designers

Listar o nome dos designers envolvidos no projeto
* Designer1
* Designer2
