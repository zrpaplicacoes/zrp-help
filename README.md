# ZRP WEBCREATIONS #

Ferramentas Inclusas

* 	Aulas HTML e CSS - Alguns pdfs com conteúdo resumido de HTML e CSS (em construção)
* 	Design - Conta com links interessantes e ferramentas de design interessantes (em construção)
* 	Git - Tudo sobre GIT organizado em um ppt e dicas
* 	Icones ZRP - Conta com ícones para sua área de trabalho
* 	Programação - Conta com links interessantes e as seguintes ferramentas
  * Sistema de abas em ajax
  * Máscaras de Formulários com JS
  * Sistema de envio de emails
  * Calendário JQuery
  * Menu Responsivo
  * Footer Fixo no Rodapé
  * ZRP Grids
  * Regex - Validação


**Para formatação de README.md, utilize o [Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#table-of-contents)**

**Para mandar um link de um arquivo de um repositório, substitua o hash do commit por "master" para se referir ao arquivo mais atual  
Exemplo:  
Troque  
https://bitbucket.org/zrpwebcreations/zrp-help/src/ab2a31b4bdc8ffb5edde771de0d9a4ce98b32e0e/Git/  
Por  
https://bitbucket.org/zrpwebcreations/zrp-help/src/master/Git/  
**