# ZRP WebCreations - Grids - V 1.1 #

## Vantages e porque não usar o Bootstrap e usar o ZRPGrids ##

O ZRP grids possui o diferencial de não querer programar por você. Isso significa que ele não vem com botões pré configurados, menus, ou outras classes que no final nem seria utilizadas. 
Para isso, nós temos outras pastas no ZRP Help que serão preenchidas com essas ferramentas futuramente.
Assim, garantimos um código mais limpo, organizado, onde temos programado apenas aquilo que utilizaremos (sendo, portanto, mais rápido também).
Além disso, o ZRPGrids inclui algumas normalizações que o Bootstrap não possui, sendo elas detalhadamente explicadas abaixo.

## O que está incluso ##

+ Modernizr para funcionamento de HTML5 em browsers antigos
+ Grids ZRP padrão 12 desenvolvida para mobile first (ler Como Funciona)
+ CSS Reset padrão para projetos
+ Margins, viewports, box-sizing e outros normalizadores
+ Meta e Ogg tags
+ Suporte para media screen IE 6 - IE 8

## Como Usar ##

Para utilização, primeiramente, baixe o repositório inteiro e use como pasta inicial para o seu projeto.

Para começar qualquer estrutura, siga os seguintes padrões:

1. Tenha uma div container que englobe toda a sua página

Exemplo:
```
#!html
<div class="container">
</div>
```
**OBS:** Essa classe pode ser aplicada no body

2. Para cada linha abra uma div (ou article, ou section, etc.) row e dentro dela preencha-a com divs (ou articles, ou sections, etc.) que possuam classe col

Exemplo:
```
#!html
<section class="row">
	<article class="col"></article>
	<aside class="col"></aside>
</section>
```
3. Preencha as dimensões escolhidas para cada tamanho de tela, sendo que a soma de cada linha **deve** dar doze. Tamanhos disponíveis:

+ xs (mobile phones) < 768px
+ sm (tables) > 768px and < 992px
+ md > 993px and < 1200px
+ lg > 1200px


Exemplo:
```
#!html
<section class="row">
	<article class="col xs-8 sm-9 md-11 lg-11"></article>
	<aside class="col xs-4 sm-3 md-1 lg-1"></aside>
</section>
```

Feito! Todos os elementos foram normalizados com box-sizing, o que significa que paddings e borders serão calculados internamente ao elemento. Para mais informações clique [aqui](http://www.w3schools.com/cssref/css3_pr_box-sizing.asp)

## Como funciona ##

### 1. Modernizr ###

O Mordernizr é uma biblioteca js que ajuda os browsers antigos a lerem e tentarem interpretar, ao máximo, as novas ferramentas introduzidas no HTML 5 e no CSS 3. Para mais informações, clique [aqui](http://modernizr.com/docs/)

### 2. Grids para mobile first ###

O conceito de mobile first é o que está sendo empregado atualmente por ser muito mais correto, leve, e consciente em relação aos mobiles. Basicamente ele se baseia na folha de estilo estar direcionada para um mobile e, através de media-screens, você incluir mais ferramentas e completar o design para que torne-se agradável para computarores também.

Um exemplo da vantagem dessa técnica se dá na seguinte situação:

Imagine que você está utilizando uma div, com um background de uma imagem de 1200px, porém você não quer que essa imagem carregue em um celular. Organizando seu css da maneira tradicional, provavelmente ele ficaria dessa forma:

```
#!css
div {
	background-image: (url: "imagemGrandeDemais.jpg");
	background-size: cover;
}

@media only media screen (max-width: 768px) {
	background-image: none;
}
```

Porém, para um browser mobile, ao ler o código aplicado a div, será necessário o download da imagem - fazendo com que a media screen não sirva para nada!! Porém, ao invertermos a situação:

```
#!css
div {
	background-image: none;
}

@media only media screen (min-width: 1200px) {
	background-image: (url: "imagemGrandeDemais.jpg");
	background-size: cover;
}
```

Garantimos o efeito desejado. Sendo assim, deve-se usar o grid.js ou um style.js desenvolvendo-se primeiro para celulares. Depois utilizando-se as media screens para adaptar o seu design mobile para tablets e computadores.

Para mais informações leia [aqui](http://tableless.com.br/mobile-first-a-arte-de-pensar-com-foco/)

Por sua vez, o controle das grids baseia-se em floats e porcentagens nas larguras (Ex: tamanho-4 possui uma largura de 33.333333% = 100%/12*4).
Um float garante essa estrutura de tabela já conhecida, com elementos lado a lado. A função da linha é basicamente limpar esse float.

**OBS:** Esse only, do media screen garante que apenas os browsers que entenderem o conceito de media screen interpretem esse código! Para os que não entendem esse conceito,não queremos que as coisas fujam do nosso controle, sendo assim, optamos pelo HTML5Shiv e os normalizadores.

### 3. CSS Reset ###

O CSS Reset nada mais é do que uma tentativa de fazer com que todas as suas estruturas, inicialmente, comportem-se da mesma maneira em todos os browsers. Sabe-se que cada browser parte de um ponto diferente, o CSS Reset minimiza essas discrepâncias.

### 4. Margins, viewports, box-sizing e outros normalizadores ###

Inicialmente, com o CSS Reset, já zeramos margins e paddings de todos os elementos. 
Em seguida, utilizamos a propriedade de box-sizing para tornar o design mais intuitivo (ler informações acima)
Foram normalizadas também todas as imagens, que possuem largura máxima de 100% e altura como auto (assim elas não distorcem e nunca ultrapassam a barreira da coluna)
Por fim, utilizou-se da meta tag de viewport para garantir que celulares mobiles abram a versão mobile. Alguns celulares mais de ponta, principalmente os Ipods, renderizam a tela como sendo de 996px, para permitir que os sites estáticos - do século passado - não fiquem feios e disproporcionais neles. Porém, utilizamos aqui as metatags e um pouco de css para garantir que em celulares mobile apareça de acordo com o planejado no design as dimensões xs.

Para IOS e alguns Android:


```
#!html

<meta name="viewport" content="width=device-width, initial-scale=1" />

```

Para windows phone (e talvez possível padronização da W3, sem o prefixo -ms- em um futuro próximo)

```
#!css

@-ms-viewport {
	width: device-width;
}

```

### 5. Meta e Ogg Tags ###

Apenas chatices burocráticas, as meta tags tem por função demarcar quem foi o autor, qual a descrição da página, quais as palavras chaves relacionadas e etc.

Para garantir uma boa aparência quando digitado o link no facebook, as Ogg tags permitem total estilização de como o link aparecerá (aquela foto e descrição), quando escrito no facebook. Saiba como editá-lo e entenda melhor [aqui](https://developers.facebook.com/docs/opengraph?locale=pt_BR)

### 6. Suporte para media screen IE6 - IE8 ###

Para terminar de blindar a 'máscara de um novo projeto', inclui-se algumas linhas de código no head que resolvem o problema da media screen (fixando o tamanho da tela) para internet explorer 8 ou menor:

```
#!html

<!--[if lt IE 9]> 
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script> 
	<link rel="stylesheet" href="css/ie.css" />
<![endif]-->

```


A primeira linha detecta caso o browser seja internet explorer 8 ou menor. Nesse caso, as media screens não seriam detectadas (e todo o conceito de mobile first cai por terra). Para isso, define-se uma largura fixa do container visando a uma maior proporcionalidade entre as dimensões exibidas, além de importar o HTML5shiv.

### 7. Extras - Dicas para uso ###

+ Defina alturas para as suas linhas! (classe row). Os elementos dentro das linhas terão altura 100% dela, porém, é editável via css (via exemplo na pasta exemplo1)

+ Entenda o conceito de clearfix (para isso serve a classe container) - Isso significa que o seu html, o seu body, terão a exatamente a altura de todos os elementos internos a ele, incluindo as divs e qualquer outro elemento com float

+ Lembre de limpar seus floats! Crie uma div com classe clear e propriedades clear:both no css e tenha controle disso (o painel do desenvolvedor ajuda bastante nisso). Um float mal colocado pode atrapalhar o relacionamento das colunas (que utilizam da propriedade float também)

+ Entre em contato caso tenha dúvidas! :)

