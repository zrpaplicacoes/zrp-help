# ZRP WebCreations - Menu Responsivo - V 1.4 #

## Vantages de usar Transitions ##

Muitos celulares, na corrida para economizar energia, acabam cortando a leitura do javascript em partes. Sendo assim, é normal que páginas que contenham muito javascript, e js pesados rodem muito bem na em browsers em um desktop, porém muito devagar em celulares. 
O objetivo deste menu responsivo é garantir uma boa usabilidade e funcionamento tanto para mobiles, quanto para desktops, e, portanto, faz bastante uso das novas propriedades adquiridas com o CSS3.

## O que está incluso ##

+ Menu Responsivo desenvolvido com o conceito de Mobile First

## Como Usar ##

1. Para utilização, primeiramente, deve existir dois elementos, um nav, que abrigará o menu, e um link, que servirá de âncora para o menu mobile.

Exemplo:
```
#!html
<div id="pullMe" class="nav-link closed"> </div> <!-- Sendo esta a âncora -->

<nav id="menu" class="col xs-12 sm-12 md-12 lg-12"> <!-- Sendo este o menu -->

</nav>
```
**OBS:** Esse menu responsivo faz uso do ZRPGrids disponível [aqui](https://bitbucket.org/zrpwebcreations/zrp-help/src/master/programacao/ZRPGrids/?at=master)

2. Dentro do seu menu inclua a lista de itens que desejar e um javascript que puxará e empurrará o menu

Menu com Links:
```
#!html
<div id="pullMe" class="nav-link closed"> </div>

<nav id="menu" class="col xs-12 sm-12 md-12 lg-12">
	<ul id="naviagation">
		<li><a href="#">Home</a></li>
		<li><a href="#">Empresa</a></li>
		<li><a href="#">Portfólio</a></li>
		<li><a href="#">Cliente</a></li>
		<li><a href="#">Contato</a></li>
	</ul>
</nav>
```
Javascript:
```
#!html
<script type="text/javascript">
	$(document).ready(function() {
		$("#pullMe").click(function() {
			if ($(this).hasClass("closed"))  {
				$("#menu ul").toggleClass("fullMenu");
				$("#pullMe").css("background-position", "0 -60px");
				$(this).removeClass("closed");
			}
			else {
				$("#menu ul").toggleClass("fullMenu");
				$("#pullMe").css("background-position", "0 0");
				$(this).addClass("closed");
			}
		});
		$("#navigation li a").on("click", function(){
			$("#menu").hasClass("fullMenu")) {
				$("#menu ul").toggleClass("fullMenu");
				$("#pullMe").css("background-position", "0 0").addClass("closed");
		}
		});
	});
</script>
```

## Como funciona e Como Estilizar ##

### Mudando a aparência do menu em geral ###

O menu em geral é estilizado através dos caminhos "#menu ul", "#menu ul li", "#menu ul li a". Foi utilizado o conceito de menu de tela inteira, porém, pode-se padronizar do jeito que quiser. [Nesse link](http://navnav.co/) encontra-se uma ampla variedade de estilizações diferentes, todas elas podendo ser construídas editando os arquivos contidos nesse repositório.
Tenha em mente, que o desenvolvimento foi baseado em Mobile First, o que significa que ao editar os caminhos citados acima você está estilizando como o menu ficaria em mobiles.

O efeito de movimento do menu se baseia nas seguintes linhas de código:

```
#!css

-moz-transform: translateX(100%);
-ms-transform: translateX(100%);
-o-transform: translateX(100%);
-webkit-transform: translateX(100%);
transform: translateX(100%);

-moz-transition: transform 1s;
-ms-transition: transform 1s;
-o-transition: transform 1s;
-webkit-transition: transform 1s;
transition: transform 1s;
```

O efeito de translação da direita para a esquerda pode ser substituído por um rotacionamento, por uma translação de cima para baixo e etc. **Lembre-se, porém, de ajustar o efeito na classe fullMenu **.

Para terminar a estilização, basta pensar em como seria o menu em telas maiores, para isso usou-se as já conhecidas media-querys. A inclusão do "!important" garante que, quando o menu for fechado em um desktop em parte responsiva (com a tela do navegador menor do que 768px), mesmo que redimensione-se a tela para ocupar um espaço maior do que 768px, ela ocupe a tela inteira.


