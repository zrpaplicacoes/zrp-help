<?php
  require_once("lib/php/controllers/page_controller.php");
?>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<title>pages teste</title>
		<link rel="stylesheet" type="text/css" href="/style/css/style.css" />
	</head>

	<body>
	<a href="/page1" class="js-page1 btn">Page1</a>
	<a href="/page2" class="js-page2 btn">Page2</a>
	<a href="/page3" class="js-page3 btn">Page3</a>
	<a href="/page4" class="js-page4 btn">Page4</a>
	<section id="js-container" style="border: 5px solid #ff4500; padding: 10px; display:none; position: absolute; width: 800px; height: 600px; top: 200px; left: 200px;"></section>
	
	<script type="text/javascript" src="/lib/js/jquery-2.1.0.min.js"></script>
	<script type="text/javascript" src="/lib/js/pages.js"></script>
	<script type="text/javascript" src="/js/pages/index.js"></script>
	</body>

</html>