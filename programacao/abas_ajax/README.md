# Abas em Ajax - v 2.0#

### Atenção ###

1-) O sistema de abas AJAX **usa Rewrite Rule**, o que significa que supõe-se que a pasta está dentro da Root do seu servidor (ou emulador) apache. Todos os arquivos, imagens e mídias devem ser importados por seus caminhos absolutos. Caso tenha dúvidas de como trabalhar com .htaccess entre em contato conosco. 

Caso queira que ele rode fora do root do servidor basta configurar o arquivo sites-available no seu servidor apache.

Exemplo:
*em /etc/apache2/sites-available/default.conf*

	- DocumentRoot /var/www/html/path/para/a/pasta/do/abas-ajax

2-) A lib inteira parte do pressuposto que o usuário leu o Clean Code. Caso queira entender a lib sem ler o Clean Code talvez as coisas fiquem meio confusas. Há esse livro na pasta de livros do ZRP Help.
Sinta-se livre para nos contatar caso queira entender melhor como a lib funciona!

----

Este é um exemplo de funcionamento do sistema:

Na página principal haverá botões que, ao serem pressionados, carregarão um conteúdo num container definido. Esses botões receberão uma classe que marcará o histórico da onde você está na página. Essa lib segue os padrões e convenções ZRP.

page1, page2, page3 e page4 são arquivos com o código html (ou PHP) da página a ser inserida na página principal.
Elas não contém as tags html, body, title, pois elas já estão na página principal.

index.php é a página principal. Deve-se notar a necessidade de se importar alguns arquivos nela, pois sem eles o sistema não funciona.

Todo o sistema está baseado em duas necessidades: carregar itens e carregar itens que tenham id específicas (exemplo, carregar notícias e carregar notícia com id 2). Para diferenciar essas funções seguiu-se o padrão de nomenclatura:

	- utilidade da função + Page
	- utilidade da função + Id

## Como utilizar ##

0. Definir o conteúdo das abas, e colocar em arquivos .php no mesmo nível que a index, sem as tags  <html>, <body>, etc. 
O conteúdo desse arquivo será inserido dentro de uma section ou equivalente.

1. Na página principal, definir um container para receber o conteúdo da aba. Esse conteúdo será alimentado por AJAX, javascript, logo, deve ter seu id precedido por um js-.

Exemplo:
```
#!html
<section id="js-container">
</section>
```
2. Na página principal, definir os botões das abas. Elas devem ser uma tag 'a', com algumas informações necessárias pré definidas:

- href deve apontar para o caminho absoluto do arquivo
- Deve conter uma classe que siga o padrao js-nomeDoArquivo.
- Deve conter uma classe que permita a demarcação de estado (que será usada no css, algo como btn)

Exemplo:

```
#!html
<a href="/home" class="js-home btn">Home</a>
```

3. Definir a array de páginas permitidas (que serão carregadas) em:
 - /lib/php/controllers/page_controller.php, linha 7
 - /js/pages/index.js, linha 10

4. Para demarcação de itens específicos (ids), precisa-se utilizar um padrão diferente de links. O link deles deve seguir a convenção RESTful (/pagina/id) e deve conter duas classes js de funcionamento, além da de estilização css:

Exemplo:

```
#!html
<a href="/novidades/4" class="js-redirect-id js-novidades-id-4 btn">Quarta novidade</a>
```

### Função change_page() ###

```
#!javascript

changePage(containerId, clickedClass, pageThatShouldBeLoaded, fadeoutTimer, fadeinTimer, type)

```

* container_id: string com o id do elemento html que receberá o conteúdo (1) - com # (css-like)
* clickedClass: string com a classe do botão clicado (2) - com # (css-like)
* fadeoutTimer: int com o tempo em ms do efeito de fadeout da aba ativa
* fadeinTimer: int com o tempo em ms do efeito de fadein da nova aba

A partir daí os argumentos que devem ser passados para a função seguem o condicinal:

- Se type for "page"

* pageThatShouldBeLoaded: string com o caminho absolute do arquivo que deve ser carregado. Edição dela está sendo feita em /pages/index.js, função formatAsAPage.

- Se type for "id"

* pageThatShouldBeLoaded : array com as infos da página que deve ser carregada. Sendo elas o caminho absoluto do arquivo (formado em /pages/index.js, função formatAsAIdPage) e o id do elemento.

5. Deve-se alterar o arquivo .htaccess para satisfazer o padrão do site. Basta dar match em todas as páginas que carregam do tipo page na primeira regra e as que carregam do tipo id na segunda regra (lembrando que páginas do tipo id também terão tipo page para ser carregado).

## Exemplo ##

Se o .php da página contém:
```
#!html
<h1>Oi</h1>
```
O container:
```
#!html
<section id="js-container">
</section>
```
Vira:
```
#!html
<section id="js-container">
<h1>Oi</h1>
</section>
```

## Agradecimentos ##

Essa lib não seria possível ser realizada sem a ajuda de:

[Rogério Yuuki Motisuki](https://br.linkedin.com/in/rogerioyuuki)
[Felipe Fernandes](https://facebook.com/fe.w.fernandes?fref=ts)
[Daniela Yassuda](https://facebook.com/daniela.yassuda?fref=ts)
[Rafael Costella](https://br.linkedin.com/pub/rafael-trostli-costella/91/21/19b)