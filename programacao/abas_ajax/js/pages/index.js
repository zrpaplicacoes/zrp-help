$(document).ready(function() {
	initializeLinks();

	initializePage();

  initializePageListenning();
});

function initializeLinks() {
  var pagesAvailable = ["page1","page2","page3","page4"];

  pagesAvailable.forEach(function(page) {
    initAjaxRedirectPage(page);
  });

  initAjaxRedirectId();
}

function initAjaxRedirectPage(page) {
  var classLinkAjaxRedirect = ".js-" + page,
      pageThatShouldBeLoaded = formatAsAPage(page);

  $(classLinkAjaxRedirect).click(function(event) {
	event.preventDefault();
	changePage('#js-container', classLinkAjaxRedirect, pageThatShouldBeLoaded, 0, 600, 'page');
  });
}

function formatAsAPage(page) {
  return "/" + page + ".php";
}

function initAjaxRedirectId(page, id) {
  $("body").on("click", ".js-redirect-id", function(event) {
    event.preventDefault();

    var id = $(this).attr("id"),
        page = $(this).attr("href").match(/\/(.*)\//)[1];

    ajaxRedirectId(page, id);
  });
}

function ajaxRedirectId(page, id) {
  var classLinkAjaxRedirect = ".js-" + page + "-id-" + id,
	  fullPath = formatAsAIdPage(page, id),
	  pageThatShouldBeLoaded = [fullPath, id];

  changePage('#js-container', classLinkAjaxRedirect, pageThatShouldBeLoaded, 0, 0, 'id');
}

function formatAsAIdPage(page, id) {
  return "/" + page + ".php?id=" + id;
}

function initializePage() {
	if (window.initialId == "") {
	  changePage('#js-container', ".js-" + window.initialPage , formatAsAPage(window.initialPage), 0, 600, 'page');
	}
	else {
	  ajaxRedirectId(window.initialPage, window.initialId);
	}
}

function initializePageListenning() {
  $(window).on("popstate", function () {
    var url = window.location.href;

    setInitialId(url);
    window.initialPage = url.match(/page\d+/)[0];

    initializePage();
  });
}

function setInitialId(url) {
  if (url.match(/.+\/\d+/) == null) {
    window.initialId = "";
  }
  else {
    window.initialId = url.match(/.+\/(\d+)/)[1];
  }
}