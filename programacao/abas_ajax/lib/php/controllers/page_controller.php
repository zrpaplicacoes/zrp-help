<?php

  initializePages();

  function initializePages() {
	$default = "page1";
	$allowedPages = array("page1","page2","page3","page4");

	if (hasOnlyPageRequest($allowedPages)) {
	  $page = $_GET['page'];
	  setJsVariables($page, "");
	}

	else if (hasPageAndIdRequest($allowedPages)) {
	  $page = $_GET['page'];
	  setJsVariables($page, $_GET['id']);
	}

	else {
		$page = $default;
    setJsVariables($page, "");
	}

  }

  function hasOnlyPageRequest($allowedPages) {
    if (isset($_GET['page']) && !isset($_GET['id'])) {
      if (in_array($_GET['page'], $allowedPages)) {
	    return true;	
      }
      return false;
    }
    return false;    
  }

  function hasPageAndIdRequest($allowedPages) {
    if (isset($_GET['page']) && isset($_GET['id'])) {
      if (in_array($_GET['page'], $allowedPages)) {
	    return true;	
      }
      return false;
    }
    return false;
  }

  function setJsVariables($page, $id) {
  	echo "<script type='text/javascript'>
  			  window.initialPage = '$page';
  			  window.initialId = '$id';
  			</script>";	
  }
?>