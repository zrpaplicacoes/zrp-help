function changePage(containerId, clickedClass, pageThatShouldBeLoaded, fadeoutTimer, fadeinTimer, type) {
  if (window.currentOpenedPage !== clickedClass) {
    window.currentOpenedPage = clickedClass;
    if (type == 'page') {
      loadPageInfo(containerId, pageThatShouldBeLoaded, fadeoutTimer, fadeinTimer);
    }

    else if (type == 'id') {
      loadPageWithIdInfo(containerId, pageThatShouldBeLoaded[0], pageThatShouldBeLoaded[1], 
                fadeoutTimer, fadeinTimer);
    }
  }

  changeButtonStatus(clickedClass);
}

function changeButtonStatus(clickedClass) {

  $(document).ajaxComplete(function() {
    $('.btn.active').removeClass('active');
    $(clickedClass).addClass('active');
  });
}

function loadPageInfo(containerId, pageThatShouldBeLoaded, fadeoutTimer, fadeinTimer) {
  var page;

  $(containerId).fadeOut(fadeoutTimer);
  $(containerId).load(pageThatShouldBeLoaded);
  $(containerId).fadeIn(fadeinTimer);

  page = window.currentOpenedPage.match(/js-(.*)/)[1];

  window.history.pushState({}, document.title , "/" + page );
}

function loadPageWithIdInfo(containerId, pageThatShouldBeLoaded, idThatShouldBeLoaded, fadeoutTimer, fadeinTimer) {
  var page;
  
  $(containerId).fadeOut(fadeoutTimer);
  $(containerId).load(pageThatShouldBeLoaded);
  $(containerId).fadeIn(fadeinTimer);

  page = window.currentOpenedPage.match(/js-(.*)-id-/)[1];

  window.history.pushState({}, document.title , "/" + page + "/" + idThatShouldBeLoaded );
}