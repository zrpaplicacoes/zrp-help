$(document).ready(function(){

  $('.telcel').keydown(function() {  
        var $elem = $(this);  
        var tamanhoAnterior = this.value.replace(/\D/g, '').length;  //para pegar o tamanho real do input
        setTimeout(function() {   
          var novoTamanho = $elem.val().replace(/\D/g, '').length;  
          
          if (novoTamanho !== tamanhoAnterior) {  
            
            if (novoTamanho === 11) {    
              
              $elem.val($elem.val().replace(/\D/g, "")).unmask();  
              $elem.mask("99-99999-9999"); 
            
            } 

            else if (novoTamanho === 10) {    
              
              $elem.val($elem.val().replace(/\D/g, "")).unmask();    
              $elem.mask("99-9999-9999?9"); 
            
            }  
            else {
              
              $elem.val($elem.val().replace(/\D/g, "")).unmask();
              
            }
          }  
        }, 1); 
      });

  $('.cnpj').keydown(function() {
        var $elem = $(this);  
        var tamanhoAnterior = this.value.replace(/\D/g, '').length;  //para pegar o tamanho real do input
        setTimeout(function() {   
        var novoTamanho = $elem.val().replace(/\D/g, '').length;  
        
        if (novoTamanho !== tamanhoAnterior) {  
      
            if (novoTamanho === 14) {    
      
              $elem.unmask();    
              $elem.mask("99.999.999/9999-99"); 

            }

            else if (novoTamanho >= 9) {

              $elem.val($elem.val().replace(/\D/g, "")).unmask();

            }
            else {

              $elem.val($elem.val().replace(/\D/g, "")).unmask();

            }
          }  
        }, 1);  
  });

  $('.cpf').keydown(function() {
        var $elem = $(this);  

        var tamanhoAnterior = this.value.replace(/\D/g, '').length;  //para pegar o tamanho real do input

        setTimeout(function() {   

          var novoTamanho = $elem.val().replace(/\D/g, '').length;  

          if (novoTamanho !== tamanhoAnterior) {  

            if (novoTamanho === 11) {    

              $elem.unmask();    

              $elem.mask("999.999.999-99"); 

            }
            else if (novoTamanho >= 8) {

              $elem.val($elem.val().replace(/\D/g, "")).unmask();

            }

            else {

              $elem.val($elem.val().replace(/\D/g, "")).unmask();

            }
          }  
        }, 1);  
  });

  
});
