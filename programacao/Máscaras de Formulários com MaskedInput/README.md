# Máscaras de Formulários com MaskedInput #

## Para que serve? ##

As máscaras em inputs permitem a organização do banco de dados do cliente de acordo com um padrão pré determinado.
Elas garantem também que o usuário do site não perca tempo escolhendo por um padrão para ser digitado ou não vá
contra os padrões do cliente. 
Essas máscaras devem ser associadas com mensagens de sucesso ou falha, dependendo do front-end do projeto.


## Padrões Disponíveis ##

1-) Telefone (e celular, padrão de nove dígitos)

2-) CNPJ

3-) CPF

## Como utilizar ##

0. Importe o arquivo jquery.maskedinput.min.js ao seu html.

```
#!js
<script type="text/javascript" src="jquery.maskedinput.min.js"></script>
```

- Importe o mascarasForm.js (dependendo do site pode ser incluso em outro js ou ser escrito inline).

```
#!js
<script type="text/javascript" src="mascarasForm.js"></script>
```

- Defina uma classe que deve receber os novos padrões no seu HTML. 

```
#!html
<input type="text" class="telcel" name="CelularSP" />
```

- Recupere esses dados no mascarasForm.js associando a mudança a ser realizada a um evento keydown. Guarde-os em uma variável local (para ser utilizada dentro de ifs).


```
#!js
	$('.telcel').keydown(function() {  
	        var $elem = $(this);   
	});
```

- Recupere o tamanho original do input (disprezando-se traços ou barras digitados pelo usuário)

```
#!js
	var tamanhoAnterior = this.value.replace(/\D/g, '').length;
```

- Defina um timeOut para conseguir recuperar o novo tamanho do input (com a tecla que ativou o evento keydown).       

```
#!js
	setTimeout(function() {   
          var novoTamanho = $elem.val().replace(/\D/g, '').length; 
    } ,1);
```

- Interprete esses dados de acordo com a necessidade.

```
#!js
	if (novoTamanho !== tamanhoAnterior) {  

	           
	    if (novoTamanho === 11) {   
			// 9 dígitos + DDD
	    } 

```

- Retire a máscara anterior e adicione a masca dentro do caso em que está trabalhando.


```
#!js
	if (novoTamanho !== tamanhoAnterior) {  

	           
	    if (novoTamanho === 11) {   
			$elem.val($elem.val().replace(/\D/g, "")).unmask();  
			$elem.mask("99-99999-9999"); 
	    } 

```

- Estilize a vontade, siga o padrão que preferir. No mascarasForm.js consta exemplos para vários casos.