# ZRP WebCreations - Regex - V 1.0 #

## Vantages de usar Regex ##

Regex servem para se capturar padrões. A vantagem delas, é que podemos facilmente validar dados em um cadastro, proporcionar uma melhor organização das nossas URLs, proteger uma pasta, além de muitas outras funções. 
É extremamente recomendado que nos ajudem preenchendo essa tabela com Regex já utilizadas e que aprendam como usá-las.
Uma boa dica, é esse site [aqui](http://regexone.com/lesson/0), que possui um tutorial bastante dinâmico e rápido.

## Expressões comumente utilizadas ##

| Texto Capturado                                   |               Regex                                    |
| --------------------------------------------------|--------------------------------------------------------|
|Id (números apenas, um no mínimo)                  |             `/^[\\d]+$/`                                | 
|Captura nome de um arquivo (a partir de uma URL)   |                `/([^\/]+)(?=\.\w+$)/`                  | 
|Captura um nome                                    |             `/^[\\p{L}\p{Zs}0-9.,&'\"]+$/`              | 
|Captura um telefone (qualquer tamanho)             |                   `/^[\d\s()-]+$/`                     | 
|Captura telefone com o padrão DD-DDDD?D-DDDD (hífens opcionais)| `/^\d{2}[-]*\d{4}\d?[-]*\d{4}$/`           | 

**Estados Brasileiro: **`/(A[CLPM]|BA|CE|DF|GO|ES|M[ATSG]|P[ABREI]|R[JNSOR]|S[PCE]|TO)/`` 