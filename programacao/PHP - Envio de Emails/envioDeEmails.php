<?php
require_once("phpmailer/PHPMailerAutoload.php"); //Carrega a biblioteca PHPMailer

try {
 	//Inicia o PHPMailer
	$mail = new PHPMailer;

	//OPCIONAL: enviar email através de SMTP (igual seu celular faz quando conecta no gmail, por exemplo)
	$mail->isSMTP();
	$mail->SMTPAuth = true;
	$mail->Host = "coloque_smtp_aqui";
	$mail->Port = 25;
	$mail->Username = "coloque_usuario_aqui";
	$mail->Password = "coloque_senha_aqui";


	//Codificação UTF-8:
	$mail->CharSet = 'UTF-8';

	//Email com HTML:
	$mail->isHTML(true);
	//Campo "From" do email:
	$mail->setFrom("email@email.com", "Email");
	//Destinatário(s):
	$mail->addAddress("email@email.com", "Email");
	//CC:
	$mail->addCC("email@email.com", "Email");
	//Assunto:
	$mail->Subject = "coloque_assunto_aqui";
	//Anexos:
	$mail->addAttachment("caminho_do_arquivo");
	//Corpo da mensagem:
	$mail->Body = "<h1>Coloque a mensagem com HTML aqui</h1>";
	//Corpo da mensagem alternativo, caso o cliente não tenha HTML
	$mail->AltBody = "Coloque a mensagem sem HTML aqui";
	//Necessário para evitar problemas (ver README):
	$mail->WordWrap = 70;

	$mail->send()
}

catch(phpmailerException $e) {
	//usado para debug, apagar pós criação do site
	echo $e->getMessage();

	//dar um tratamento de erros aqui
	}
