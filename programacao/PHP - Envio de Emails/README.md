# PHP - Envio de Email - V 1.2 #

O phpmailer é uma ferramente simples de envio de email. 

Vantagem: facilmente editável e simples para fazer até mesmo coisas complicadas.

Exemplo: envio por SMTP, anexos, wordwrap automático, etc…

## Como utilizar ##

0. É preciso primeiro importar a biblioteca. Copie a pasta phpmailer e seus arquivos. Depois inclua no seu código:

```
#!php
require_once("phpmailer/PHPMailerAutoload.php");
```

1. Crie uma variável para inicializar a classe PHPMailer:

```
#!php
$mail = new PHPMailer;
```

2. (OPCIONAL) Você pode enviar o email através de um servidor SMTP, da mesma maneira que seu celular ou tablet faz quando se conecta à sua conta de email. Caso você decida pular este passo, você precisa de um servidor de email local instalado, como o Sendmail para Linux.

```
#!php
$mail->isSMTP();
$mail->SMTPAuth = true;
$mail->Host = "coloque_smtp_aqui";
$mail->Port = 25;
$mail->Username = "coloque_usuario_aqui";
$mail->Password = "coloque_senha_aqui";
```

3. Definir a codificação como UTF-8, para evitar problemas com acentos.

```
#!php
$mail->CharSet = 'UTF-8';
```

4. Definir todos os parâmetros do email, como destinatário, assunto, mensagem, etc…

```
#!php
//Email com HTML:
$mail->isHTML(true);
//Campo "From" do email:
$mail->setFrom("email@email.com", "Email");
//Destinatário(s):
$mail->addAddress("email@email.com", "Email");
//CC:
$mail->addCC("email@email.com", "Email");
//Assunto:
$mail->Subject = "coloque_assunto_aqui";
//Anexos:
$mail->addAttachment("caminho_do_arquivo");
//Corpo da mensagem:
$mail->Body = "<h1>Coloque a mensagem com HTML aqui</h1>";
//Corpo da mensagem alternativo, sem o HTML
$mail->AltBody = "Coloque a mensagem sem HTML aqui";
```

5. É necessário definir um Word Wrap, ou seja, um tamanho máximo de uma linha. O número máximo de caracteres em uma linha permitido é 78, como definido no [RFC 2822](http://tools.ietf.org/html/rfc2822#section-2.1.1). Caso isso não seja feito, pode haver problemas em alguns clientes de email.

Por sorte, o PHPMailer faz isso automaticamente para nós, basta uma linha:

```
#!php
$mail->WordWrap = 70;
```

6. Por último o envio do email. Todo o processo é envolvido por um tratamento de erro do tipo Try-Throw-Catch. O phpmailerException não é capaz de tratar todos os tipos de Exception, porém, ele exibe uma mensagem mais explicativas quando trata, por isso, fazemos dois catchs, priorizando o phpmailerException. Depois de capturar o erro, faça o que quiser com ele. Neste caso estou dando um echo. Mas não mostre o erro para o usuário, claro.

```
#!php
try {
.
.
.
$mail->send()

}
catch(phpmailerException $e) {
	//usado para debug, apagar pós criação do site
	echo $e->getMessage();

	//dar um tratamento de erros aqui
	}
catch (Exception $e) {
	//usado para debug, apagar pós criação do site
	echo $e->getMessage();
  	//dar um tratamento de erros aqui
}

```