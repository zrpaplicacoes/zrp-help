<?php
    class User extends ActiveRecord\Model {

        static $has_many = array (
          array('logins')
        );

        static $belongs_to = array (
          array('role')
        );

        static $validates_presence_of = array (
           array('email', 'message' => 'email é obrigatório'),
           array('hash', 'message' => 'senha é obrigatório'),
           array('name', 'message' => 'nome é obrigatório'),
           array('lastname',  'message' => 'sobrenome é obrigatório')
        );

        static $validates_uniqueness_of = array (
          array('email',  'message' => 'email já registrado')
        );

        static $validates_format_of = array (
          array('email',
                   'with' => '/^[^0-9][A-z0-9_]+([.][A-z0-9_]+)*[@][A-z0-9_]+([.][A-z0-9_]+)*[.][A-z]{2,4}$/',
                   'message' => 'formato de email não válido')
        );

        static $validates_length_of = array (
             array('hash', 'minimun' => 7, 'within' => array(7,20),
                      'too_short' => 'a senha deve ter no mínimo 7 caracteres',
                      'too_long' => 'a senha deve ter no máximo 20 catacteres')
        );
    }

