<?php
    class Role extends ActiveRecord\Model {
        static $has_many = array (
          array('users')
        );

        static $validates_presence_of = array (
           array('level'),
           array('name', 'message' => 'o nível deve ter um nome')
        );

        static $validates_numericality_of = array (
            array('level', 'greater_than_or_equal_to' => 0, 'message' => "precisa ser no mínimo zero")
        );
    }

