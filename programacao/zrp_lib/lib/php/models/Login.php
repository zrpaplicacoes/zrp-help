<?php
    class Login extends ActiveRecord\Model {
      const MAX_USER_ATTEMPTS = 5;
      const MAX_USER_ATTEMPTS_INTERVAL_IN_MINUTES = 30;

      static $before_save = array('normalizeIp');

      static $belongs_to = array (
        array('user')
      );

      static $validates_presence_of = array (
        array('ip'),
        array('succeeded', 'in' => array(true, false)),
      );

      static $validates_numericality_of = array( 
        array('ip', 'only_integer' => true, 'message' => 'O ip deve ser um número')
      );

      public function normalizeIp() {
        $this->ip = str_replace("%.%", "", $this->ip);
        $this->ip = str_replace("%,%", "", $this->ip);
      }

      public function validate() {
        $failureAttempts = self::count(array('conditions' => array('user_id = ? AND succeeded = ? AND date >  DATE_SUB(NOW(), INTERVAL ? MINUTE)', $this->user_id, $this->succeeded, self::MAX_USER_ATTEMPTS_INTERVAL_IN_MINUTES)));
        if ($failureAttempts > self::MAX_USER_ATTEMPTS) {
          $this->errors->add('loginMaximumAttempts', 'você excedeu o limite de tentativas, por favor, cheque seu email para definir uma nova senha');
        }
      }
}

