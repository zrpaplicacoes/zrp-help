<?php
//Sempre usar essa função no lugar de session_start()
//Torna o cookie HTTP Only
function start_session() {
	$session_time_out = 3600; //Segundos

	$cookieParams = session_get_cookie_params();
	session_set_cookie_params($cookieParams["lifetime"],
	       $cookieParams["path"], 
	       $cookieParams["domain"], 
	       false,
	       true);
	session_start();

	//Reseta o token temporário e gera um novo, caso não exista.
	if (!isset($_SESSION['token'])) {
		CSRF::renewToken();
	}
	if (isset($_SESSION['timeout_token'])) {
		unset($_SESSION['timeout_token']);
	}

	//Checagem de timeout:
	if (!isset($_SESSION['last_activity'])) {
		$_SESSION['last_activity'] = time();
	}
	elseif (time() - $_SESSION['last_activity'] > $session_time_out) {
		$token = $_SESSION['token'];

		//Destroi a sessão e recria novamente.
		$_SESSION = array();
		setcookie(session_name(), "", time() - 3600, "/");
		session_destroy();
		$cookieParams = session_get_cookie_params();
		session_set_cookie_params($cookieParams["lifetime"],
		       $cookieParams["path"], 
		       $cookieParams["domain"], 
		       false,
		       true);
		session_start();
		CSRF::renewToken();
		$_SESSION['last_activity'] = time();
		$_SESSION['timeout_token'] = $token;
	}
	else {
		$_SESSION['last_activity'] = time();
	}

}
start_session();
//SEMPRE use essa função quando for colocar uma variável que veio do usuário num HTML.
function html_sanitize($input) {
	return htmlspecialchars($input, ENT_QUOTES, "UTF-8");
}
//Verificação contra CSRF
class CSRF {
	//512 bit token
	private $token;

	public function __construct() {
		$this->token = self::getToken();
	}

	public static function getToken() {
		//Retorna o token da sessão. Ou gera o token da sessão.
		if (isset($_SESSION['token']) && strlen($_SESSION['token']) === 64) {
			return $_SESSION['token'];
		}
		else {
			$token = self::generateToken();
			$_SESSION['token'] = $token;
			return $token;
		}
	}

	public static function renewToken() {
		$token = self::generateToken();
		$_SESSION['token'] = $token;
	}

	public static function verify($token) {
		if (isset($_SESSION['token']) and strlen($token) === 64 and $_SESSION['token'] === $token) {
			return true;
		}
		elseif (isset($_SESSION['timeout_token'])) {
			$verified = $_SESSION['timeout_token'] === $token;
			unset($_SESSION['timeout_token']);

			return $verified;
		}
		else {
			return false;
		}
	}

	public static function generateInput() {
		//Gera o HTML do input para ser colocado nos forms.
		$token = self::getToken();
		$output = "<input type='hidden' name='token' value='$token'>";
		return $output;
	}

	public static function generateToken() {
		//Gera 64 caracteres a partir de 48 bytes pseudo-aleatórios.
		//Tenta gerar com algoritmos criptograficamente seguros, com fallback para mt_rand().
		$raw_buffer_len = 48;

		$buffer = '';
		$buffer_valid = false;
		if (function_exists('mcrypt_create_iv') && !defined('PHALANGER')) {
		    $buffer = mcrypt_create_iv($raw_buffer_len, MCRYPT_DEV_URANDOM);
		    if ($buffer) {
		        $buffer_valid = true;
		    }
		}
		if (!$buffer_valid && function_exists('openssl_random_pseudo_bytes')) {
		    $buffer = openssl_random_pseudo_bytes($raw_buffer_len);
		    if ($buffer) {
		        $buffer_valid = true;
		    }
		}
		if (!$buffer_valid && @is_readable('/dev/urandom')) {
		    $f = fopen('/dev/urandom', 'r');
		    $read = PasswordCompat\binary\_strlen($buffer);
		    while ($read < $raw_buffer_len) {
		        $buffer .= fread($f, $raw_buffer_len - $read);
		        $read = PasswordCompat\binary\_strlen($buffer);
		    }
		    fclose($f);
		    if ($read >= $raw_buffer_len) {
		        $buffer_valid = true;
		    }
		}
		if (!$buffer_valid || PasswordCompat\binary\_strlen($buffer) < $raw_buffer_len) {
		    $bl = PasswordCompat\binary\_strlen($buffer);
		    for ($i = 0; $i < $raw_buffer_len; $i++) {
		        if ($i < $bl) {
		            $buffer[$i] = $buffer[$i] ^ chr(mt_rand(0, 255));
		        } else {
		            $buffer .= chr(mt_rand(0, 255));
		        }
		    }
		}

		$token = base64_encode($buffer);
		return $token;
	}
}

function getCSRF() {
  if (isset($_POST['token'])) {
    return $_POST['token'];
  }
}


function validateCSRF($CSRF) {
  if (!CSRF::verify($CSRF)) {
    return false;
  }
  return true;
}