<?php
ActiveRecord\Config::initialize(function($cfg) {

  $cfg->set_model_directory("lib/php/models/");
  $cfg->set_connections( array(
	                            'development' => 'mysql://zrp:zrp@localhost/mydb?charset=utf8',
	                            'production' => 'mysql://username:password@localhost/production_database_name?charset=utf8'
	                             )
	                  );

  $cfg->set_default_connection('development');
});
