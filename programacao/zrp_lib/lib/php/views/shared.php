<?php
  function messageFor($type) {
  	if (hasFlashMessage()) {
  	  if ($type == 'submit') {
  	  	echo "<span>" . failedFormSession() . "</span>";
  	  	unsetFlashMessage(); 
  	  }
  	  else {
  	  	echo "<span>" . inputErrorMessage($type) . "</span>";
  	  }
  	}
  }

  function failedFormSession() {
  	if (isset($_SESSION['formError'])) {
  	  echo $_SESSION['formError'];
  	}
  }

  function inputErrorMessage($type) {
  	if (isset($_SESSION['dataErrors'])) {
  	  $errorObject = json_decode($_SESSION['dataErrors']);
  	  echo $errorObject[$type];
  	}
  }
