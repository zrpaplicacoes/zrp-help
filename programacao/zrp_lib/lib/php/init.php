<?php
  require_once "lib/php/tools/php-activerecord/ActiveRecord.php";
  require_once "lib/php/tools/connection.php";

  require_once "lib/php/tools/password_compat.php";
  require_once "lib/php/tools/security.php";

  require_once "lib/php/controllers/shared_controller.php";
  require_once "lib/php/controllers/login_controller.php";

  require_once "lib/php/views/shared.php";
