<?php
  function setFlashMessage() {
  	$_SESSION['flash'] = true;
  }

  function unsetFlashMessage() {
  	$_SESSION['flash'] = null;
  }

  function hasFlashMessage() {
  	if (isset($_SESSION['flash'])) {
  	  return true;
  	}
  	return false;
  }