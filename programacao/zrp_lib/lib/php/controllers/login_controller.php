<?php

function authenticate($email, $hash) {
  $user = User::find(array('conditions' => array('email = ?', $email)));
  setFlashMessage();
  if ($user) {
    if (password_verify($hash, $user->hash)) {
      $loginAttempt = registerAttemptInDatabase($user, true);
    }
    else {
      $loginAttempt = registerAttemptInDatabase($user, false);
      setLoginFailedSession();
    }
    $_SESSION['dataErrors'] = serialize($loginAttempt->errors);
    redirectToHome();
  }
  else {
  	setLoginFailedSession();
  	redirectToHome();
  }
}

function registerAttemptInDatabase($user, $status) {
  
  $ip = getUserIp();

  $login = new Login();
  $login->ip = $ip;
  $login->succeeded = $status;
  $login->user_id = $user->id;

  if ($login->is_valid()) {
    $login->save();
  }

  return $login;
}

function getUserIp() {
  if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
      $ip = $_SERVER['HTTP_CLIENT_IP'];
  }
  else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
      $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
  }
  else {
      $ip = $_SERVER['REMOTE_ADDR'];
  }

  return $ip;
}

function setLoginFailedSession() {
  $_SESSION['formError'] = "usuário e/ou senha errados";
}

function logout() {
  $_SESSION = array();
  setcookie(session_name(), "", time() - 3600, "/");
  session_destroy();
}