<?php

  require_once "./lib/php/init.php";

  if (isset($_POST['action'])) {
    $CSRF = getCSRF();
    if (validateCSRF($CSRF)) {
      evaluateAction($_POST['action']);
    }
    else {
      redirectToHome();
    }
  }

  function evaluateAction($action) {
    switch($action) {
      case "login":
        login();
      break;
    }
  }

  function redirectToHome() {
    header("Location: /");
    exit();
  }

  function login() {
    if (isset($_POST['email'], $_POST['hash'])) {
      authenticate($_POST['email'], $_POST['hash']);
    }
    else {
      redirectToHome();
    }
  }
?>