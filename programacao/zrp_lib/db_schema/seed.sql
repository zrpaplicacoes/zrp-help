SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `mydb` ;
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`roles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`roles` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `level` INT NOT NULL,
  `name` VARCHAR(65) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `level` (`level` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(85) NOT NULL,
  `lastname` VARCHAR(85) NOT NULL,
  `email` VARCHAR(100) NOT NULL,
  `hash` VARCHAR(85) NOT NULL,
  `password_verification_token` VARCHAR(85) NOT NULL,
  `password_verification_time` VARCHAR(85) NOT NULL,
  `role_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_users_roles_idx` (`role_id` ASC),
  CONSTRAINT `fk_users_roles`
    FOREIGN KEY (`role_id`)
    REFERENCES `mydb`.`roles` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`logins`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`logins` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ip` VARCHAR(45) NOT NULL,
  `succeeded` TINYINT(1) NOT NULL,
  `user_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_logins_users_idx` (`user_id` ASC),
  CONSTRAINT `fk_logins_users`
    FOREIGN KEY (`user_id`)
    REFERENCES `mydb`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`news`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`news` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `description` TEXT NOT NULL,
  `status` INT NOT NULL,
  `date` TIMESTAMP NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`authors`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`authors` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `new_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_authors_users_idx` (`user_id` ASC),
  INDEX `fk_authors_news_idx` (`new_id` ASC),
  CONSTRAINT `fk_authors_users`
    FOREIGN KEY (`user_id`)
    REFERENCES `mydb`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_authors_news`
    FOREIGN KEY (`new_id`)
    REFERENCES `mydb`.`news` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`images`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`images` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `url` VARCHAR(255) NOT NULL,
  `label` VARCHAR(120) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`imageNews`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`imageNews` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `new_id` INT NOT NULL,
  `image_id` INT NOT NULL,
  `position` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_imagenews_images_idx` (`image_id` ASC),
  INDEX `fk_imagenews_news_idx` (`new_id` ASC),
  CONSTRAINT `fk_imagenews_news`
    FOREIGN KEY (`new_id`)
    REFERENCES `mydb`.`news` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_imagenews_images`
    FOREIGN KEY (`image_id`)
    REFERENCES `mydb`.`images` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`universities`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`universities` (
  `id` INT NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`registrations`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`registrations` (
  `id` INT NOT NULL,
  `birthdate` DATE NOT NULL,
  `gender` VARCHAR(30) NOT NULL,
  `shoe_number` FLOAT NOT NULL,
  `rg` INT NOT NULL,
  `university_id` INT NOT NULL,
  `user_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_resgistrations_users_idx` (`user_id` ASC),
  INDEX `fk_resgistrations_universities_idx` (`university_id` ASC),
  CONSTRAINT `fk_resgistrations_users`
    FOREIGN KEY (`user_id`)
    REFERENCES `mydb`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_resgistrations_universities`
    FOREIGN KEY (`university_id`)
    REFERENCES `mydb`.`universities` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`events`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`events` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `place` VARCHAR(255) NOT NULL,
  `description` TEXT NOT NULL,
  `participant_limitation` INT NOT NULL,
  `status` INT NOT NULL,
  `date` TIMESTAMP NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `date` (`date` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`eventImages`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`eventImages` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `event_id` INT NOT NULL,
  `image_id` INT NOT NULL,
  `position` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_eventImages_events_idx` (`event_id` ASC),
  INDEX `fk_eventImages_images_idx` (`image_id` ASC),
  CONSTRAINT `fk_eventImages_events`
    FOREIGN KEY (`event_id`)
    REFERENCES `mydb`.`events` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_eventImages_images`
    FOREIGN KEY (`image_id`)
    REFERENCES `mydb`.`images` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`specialGuests`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`specialGuests` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`speakers`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`speakers` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `event_id` INT NOT NULL,
  `specialGuests_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_speakers_events_idx` (`event_id` ASC),
  INDEX `fk_speakers_specialGuests_idx` (`specialGuests_id` ASC),
  CONSTRAINT `fk_speakers_events`
    FOREIGN KEY (`event_id`)
    REFERENCES `mydb`.`events` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_speakers_specialGuests`
    FOREIGN KEY (`specialGuests_id`)
    REFERENCES `mydb`.`specialGuests` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`enrolls`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`enrolls` (
  `id` INT NOT NULL,
  `status` INT NOT NULL,
  `presence` TINYINT(1) NOT NULL,
  `user_id` INT NOT NULL,
  `event_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_enrolls_users_idx` (`user_id` ASC),
  INDEX `fk_enrolls_events_idx` (`event_id` ASC),
  CONSTRAINT `fk_enrolls_users`
    FOREIGN KEY (`user_id`)
    REFERENCES `mydb`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_enrolls_events`
    FOREIGN KEY (`event_id`)
    REFERENCES `mydb`.`events` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
