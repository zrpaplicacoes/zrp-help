<?php
  require_once "lib/php/init.php";
?>
<!DOCTYPE html>
<html>
<head>
  <title>ZRP Lib</title>
  <meta charset="UTF-8" />
</head>
<body>
  <form method="POST" action="/routes.php">
	<input type="email" name="email" />
	<?php messageFor('email'); ?>
	<input type="password" name="hash" />
	<?php echo CSRF::generateInput(); ?>
	<input type="hidden" name="action" value="login" />
	<input type="submit" value="Entrar" />
	<?php messageFor('submit'); ?>
  </form>
</body>
</html>