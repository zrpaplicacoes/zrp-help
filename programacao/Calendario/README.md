# ZRP WebCreations - Calendário - V 1.0 #

## O que esse plugin oferece? ##

O calendário construído com esse plugin aqui disponível possui, entre diversas outras funcionalidades, a capacidade de exibir informações em diversos idiomas, de relacionar eventos para click, para mouseover, separação por dia, semana, mês e uma outra infinidade de atributos possíveis de serem passados.

Caso tenha que usá-lo, vale a pena perder um pouco de tempo lendo [essa informações](http://fullcalendar.io/docs/), para saber como melhor estilizar de acordo com a sua necessidade.

## O que está incluso ##

+ Calendário responsivo incluído no ZRPGrids

## Como Usar ##

1. Para utilização, primeiramente, deve-se importar o css e os js necessários

Exemplo:
```
#!html
<head>
	<link rel='stylesheet' href='css/calendar.css' />
</head> 
<body>

</body>
<script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
<script type="text/javascript" src='js/moment.min.js'></script>
<script type="text/javascript" src='js/calendar.min.js'></script>
<script type="text/javascript" src='js/calendarLangAll.js'></script> <!-- Necessário para mudança de idioma do calendário -->
```
2. Crie um elemento que sediará o calendário, e utilize do JQuery para ativá-lo

Elemento:
```
#!html
<section id='calendar' class="col xs-12 sm-12 md-10 lg-10"></section>
```
Javascript:
```
#!html
<script type="text/javascript">
$(document).ready(function() {
			$('#calendar').fullCalendar({

			});
</script>
```

3. A partir daí, estilize e utilize os métodos fornecidos pelo plugin [nesse link](http://fullcalendar.io/docs/). Nesse repositório encontra-se um exemplo já construído, com alguns métodos já empregados.

## Curiosidade: Complementando o plugin ##

### Criando uma div para guardar o evento ###

Como o plugin é inteiro em JS e de fácil configuração, compensa personalizar ainda mais o seu funcionamento. Como exemplo, está o calendário que será utilizado no novo sistema da ZRP, no qual cada evento está atrelado a uma lista de informações (como foi utilizado o método de arrays podemos ir além das informações fornecidas pelo plugin e guardar complementos na array, que pode ser alimentada, inclusive, por um banco de dados, como será feito futuramente no site).

Na array do arquivo em exemplo passa-se não só os parâmetros color, title, start e end, como também cliente, tarefa e prioridade.

Basicamente tem-se por objetivo exibir uma tabela quando clica-se em um evento. Para isso montou-se primeiro o código html:

```
#!html

<article id="fullInformation">
		<table>
			<thead>
				<tr>
					<th> Nome do Cliente: </th>
					<th> Status: </th>
					<th> <img src="imagens/close.png" id="close" alt="Close" /> Tarefa: </th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td id="cliente"></td>
					<td id="prioridade"></td>
					<td id="tarefa"></td>
				</tr>
			</tbody>
		</table>
</article>
```
*Deixou-se a segunda linha da tabela para ser preenchida dinamicamente

Depois, associou-se o desaparecimento e aparecimento da tabela com css:
```
#!css

#fullInformation {
	position:fixed;
	width: 100%;
	height: 0%;
	background-color: #FFF;
	z-index:9; /* Para garantir uma prioridade em cima de qualquer outra informação que possa ser adicionada na página depois */
	opacity: 0;
	overflow:hidden; /* Para além de esconder o elemento garantir que nenhum dos seus filhos ocupem algum espaço qualquer na página */

	-webkit-transition: all 2s ease; /*Transitions para não gerar uma transição brusca */
	-moz-transition: all 2s ease;
	-o-transition: all 2s ease;
	transition: all 2s ease;

}

#fullInformation.show {
	height:100%;
	opacity: 0.9;
}

#fullInformation table {
	position:relative;
	top: 10%;
	left: 20%;
	width: 60%;
	height: 10%;
	background:rgb(34,49,63);

	border:solid 3px #FFF;
	opacity: 1 !important; /*Garante que seus filhos terão opacidade 1 também*/
}

#fullInformation table th, #fullInformation table td {
	border: solid 2px #FFF;
	height:2em;
	padding: 0.5em;
	text-align:center;
	font-size: 1em;
	vertical-align: middle;
	color: #FFF;
}

#close {
	position:absolute;
	width:20px;
	height:auto;
	right:-6px;
	top:-10px;
	cursor:pointer;
}
```

E por fim, uniu-se tudo com um js, utilizando o método fornecido pelo plugin eventClick()


```
#!html

	<script>
		

		$(document).ready(function() {
			
			$('#calendar').fullCalendar({
				lang: 'pt-br',
					
					.
					.
					.

				eventClick: function(event, jsEvent, view) {
						$("#fullInformation").addClass("show");
						//mudança ativará o transition
						
						$("#cliente").html(event.cliente);
						$("#cliente").css("color", event.color);
						$("#prioridade").html(event.prioridade);
						if (event.prioridade == 'urgente') {
							$("#prioridade").css("color", "red");
						}
						else {
							$("#prioridade").css("color","green");
						}
						
						$("#tarefa").html(event.tarefa);

				}
			});

		$("#close").click(function() {
			$("#fullInformation").removeClass("show");
		});
			
		});


	</script>
```