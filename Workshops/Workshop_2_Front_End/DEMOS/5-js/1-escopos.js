var year, car;

year = 1994;
car = {
	name: "Parati",
	year: 1996
}

function change1(arg) {
	arg = 2012;
}

function change2(arg) {
	arg.year = 2012;
}

function change3(arg) {
	arg = {
		year: 555
	}
}

change1(year);
change1(car.year);

console.log(year);
console.log(car.year);

change2(car);

console.log(year);
console.log(car.year);

change3(car.year);

console.log(year);
console.log(car.year);