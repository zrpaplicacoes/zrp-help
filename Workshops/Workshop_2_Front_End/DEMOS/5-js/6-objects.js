function User(email, password) {
	this.email = email;
	this.password = password;
	var privateFunction = function() {
		console.log("I'm private");
	}
}

User.prototype = {
	checkPassword : function() {
		var checkSecurity = (/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/);
		return checkSecurity.test(this.password);
	}
}

user = new User("rafcostella@gmail.com", "testing");

user.checkPassword();

user.password = "teTS12$5%";

user.checkPassword();