/* Part I */

function zrp(thing) {
	console.log("there is a " + thing + " here");
}

zrp("snake");

zrp.call(undefined, "snake");

/* Part II */

function zrpObject(thing) {
	console.log(thing);
	this.zrp();
}

var something = {
	zrp: function() {
			console.log("is the best company in the world")
		}
}

zrpObject.call(something, "zrp");

/* Part III */

var anotherThing = {
	name: "ZRP",
	zrp: function(something) {
		console.log(this.name + " says hello to " + something);
	}
}

var tryToDefineMe = function(thing) {
	return anotherThing.zrp.call(anotherThing, thing);
}

tryToDefineMe("world");