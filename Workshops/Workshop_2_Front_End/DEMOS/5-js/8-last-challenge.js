function userIdCreator (allUsers) {
    var i, uniqueID = 100;

    for (i = 0; i < allUsers.length; i++) {
        allUsers[i].id = function() {
            return uniqueID + i;
        }
    }

    return allUsers;
}

var allUsers = [
                            {name: "Z", id: 0},
                            {name: "R", id: 0},
                            {name: "P", id: 0}
                        ];

var createIdForUsers = userIdCreator(allUsers);

console.log(createIdForUsers[0].id());
console.log(createIdForUsers[1].id());
console.log(createIdForUsers[2].id());

console.log("---- Bugou -----");