var z = 1;

function zrp() {
	console.log(z);

	if(!z) {
		var z = 10;
	}

	console.log(z);
}

zrp();
console.log(z);

/* PART II */

var r = 1;

console.log(r);

if(true) {
	var r = 2;
	console.log(r);
}

console.log(r);