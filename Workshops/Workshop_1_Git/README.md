# Comandos básicos do Git #

Git é um sistema de controle de versão distribuído (DVCS) que permite o trabalho em equipe, colaborativo e descentralizado. Sua utilização é muito simples,
sendo possível utilizá-lo através do Git Bash/Prompt do Windows, ou através de GUI's (Graphical User Interface) especiais para ele.

## Como utilizar ##

* Cria-se um diretório no disco local com um repositório git ou clona-se um repositório remoto ou pega-se os arquivos de um repositório já clonado
* Adiciona-se os arquivos para rastreamento 
* As mudanças são então commitadas para o repositório
* Feitas as mudanças, damos push para o repositório remoto
* Se quisermos ver como anda o projeto, podemos ver o log

##Links úteis:

* http://git-scm.com/book/pt-br/v1 (Livro do Git)
* https://try.github.io/ (Simulador de Bash)

##Comandos básicos##

Define o nome global de usuário
```
#!git
$ git config --global user.name “Nome”
```
Define o email global de usuário
```
#!git
$ git config --global user.email email@zrp.com.br
```
Lista todas as configurações
```
#!git
$ git config --list
```
Help do Git
```
#!git
$ git help <comando>
```
Inicializa um repositório clean localmente
```
#!git
$ git init
```
Define os arquivos a serem monitorados
```
#!git
$ git add <arquivo>
```
Commit
```
#!git
$ git commit –m “Commit inicial”
```
Estado dos arquivos
```
#!git
$ git status
```
Clona de um repositório remoto e cria a pasta com o nome Pasta
```
#!git
$ git clone [url] “Pasta” 
```
Comparar o arquivo do último checkout com o arquivo modificado
```
#!git
$ git diff
```
Adiciona automaticamente os arquivos monitorados (-a de -add)
```
#!git
$ git commit –a –m “mensagem”
```
Gera o log dos commits e tags realizado
```
#!git
$ git log
```
Remove um arquivo
```
#!git
$ git rm <arquivo>
```
Renomeia um arquivo
```
#!git
$ git mv nome_original nome_final
```
Substitui o último commit pelo novo commit
```
#!git
$ git commit –amend
```
Tira um arquivo da área de preparação
```
#!git
$ git reset HEAD <arquivo>
```
Volta o arquivo ao estado original antes da modificação
```
#!git
$ git checkout -- <arquivo>
```
Acessa as configurações de remote e mostra as urls
```
#!git
$ git remote –v
```
Adiciona um servidor remoto com o nome origin
```
#!git
$ git remote add origin [url]
```
Pega os arquivos do origin sem mesclar com a branch master
```
#!git
$ git fetch origin
```
Pega os arquivos do origin e mescla com a branch master
```
#!git
$ git pull origin master
```
Enviar ao servidor remoto os commits
```
#!git
$ git push origin master
```
Acessa todas as informações do remoto origin
```
#!git
$ git show remote origin
```
Mostra todas as tags
```
#!git
$ git tag
```
Criar uma tag anotada
```
#!git
$ git tag –a vx.x –m “mensagem”
```
Ver o contéudo de uma tag
```
#!git
$ git show vx.x
```
Pushar uma tag ao remoto origin
```
#!git
$ git push origin vx.x
```
Pushar todas as tags ao remote origin
```
#!git
$ git push origin --tags
```	